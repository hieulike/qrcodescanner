//
//  QRCodeScannerController.swift
//  QRCodeReader
//
//  Created by HieuVD on 12/19/19.
//  Copyright © 2019 Vnext Da Nang. All rights reserved.
//

import UIKit
import AVFoundation
protocol QRCodeScannerControllerDelegate: class {
    func didFindQRCode(result: String)
}
class QRCodeScannerController: UIViewController {
    
    // MARK: - Properties
    /// A switch camera button.
    lazy var switchCameraButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "ic_switch"), for: .normal)
        button.addTarget(self, action: #selector(switchCameraAction), for: .touchUpInside)
        
        return button
    }()
    
    /// A toggle torch button.
    lazy var toggleTorchButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "ic_flash_on"), for: .normal)
        button.addTarget(self, action: #selector(toggleTorchAction), for: .touchUpInside)
        return button
    }()
    /// A exit button.
    lazy var exitButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "ic_exit"), for: .normal)
        button.addTarget(self, action: #selector(toggleExitAction), for: .touchUpInside)
        return button
    }()
    
    private var overLayView: OverlayView!
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    /// Flag to know whether the scanner should stop scanning when a code is found.
    public var isShowCodeFrame = false
    public var stopScanningWhenCodeIsFound: Bool = true
    public var qrCodeFrameView: UIView?
    public var rectOfInterest: CGRect?
    public var sizeOfInterest: CGSize? {
        didSet {
            if let sizeOfInterest = sizeOfInterest {
                self.rectOfInterest = CGRect(origin: CGPoint(x: UIScreen.main.bounds.midX - sizeOfInterest.width/2, y:  UIScreen.main.bounds.midY - sizeOfInterest.height/2), size: sizeOfInterest)
            }
        }
    }
    
    private let defaultDevice: AVCaptureDevice? = AVCaptureDevice.default(for: .video)
    private let frontDevice: AVCaptureDevice?   = {
        if #available(iOS 10, *) {
            return AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .front)
        }
        else {
            for device in AVCaptureDevice.devices(for: AVMediaType.video) {
                if device.position == .front {
                    return device
                }
            }
        }
        
        return nil
    }()
    
    lazy var defaultDeviceInput: AVCaptureDeviceInput? = {
        guard let defaultDevice = defaultDevice else { return nil }
        
        return try? AVCaptureDeviceInput(device: defaultDevice)
    }()
    
    lazy var frontDeviceInput: AVCaptureDeviceInput? = {
        if let _frontDevice = self.frontDevice {
            return try? AVCaptureDeviceInput(device: _frontDevice)
        }
        
        return nil
    }()
    
    private let session        = AVCaptureSession()
    private let metadataOutput = AVCaptureMetadataOutput()
    private let supportedCodeTypes = [AVMetadataObject.ObjectType.qr]
    
    weak var delegate: QRCodeScannerControllerDelegate?
    // MARK: - LifeCycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureDefaultComponents(withCaptureDevicePosition: .back)
        self.startCameraSession()
        self.setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let connection =  self.videoPreviewLayer?.connection  {
            let currentDevice: UIDevice = UIDevice.current
            let orientation: UIDeviceOrientation = currentDevice.orientation
            let previewLayerConnection : AVCaptureConnection = connection
            
            if previewLayerConnection.isVideoOrientationSupported {
                switch (orientation) {
                case .portrait:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                case .landscapeRight:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeLeft)
                    break
                case .landscapeLeft:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .landscapeRight)
                    break
                case .portraitUpsideDown:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portraitUpsideDown)
                    break
                default:
                    updatePreviewLayer(layer: previewLayerConnection, orientation: .portrait)
                    break
                }
            }
        }
    }
    
    // MARK: - Helper methods
    private func setupUI() {
        self.sizeOfInterest = CGSize(width: 200, height: 200)
        self.overLayView = OverlayView(frame: UIScreen.main.bounds, focusRect: self.rectOfInterest)
        
        self.view.addSubview(overLayView)
        self.view.addSubview(switchCameraButton)
        self.view.addSubview(toggleTorchButton)
        self.view.addSubview(exitButton)
        
        self.view.bringSubviewToFront(switchCameraButton)
        self.view.bringSubviewToFront(toggleTorchButton)
        self.view.bringSubviewToFront(exitButton)
        self.layoutComponents()
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView, isShowCodeFrame {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubviewToFront(qrCodeFrameView)
        }
    }
    
    private func configureDefaultComponents(withCaptureDevicePosition: AVCaptureDevice.Position) {
        for output in session.outputs {
            session.removeOutput(output)
        }
        for input in session.inputs {
            session.removeInput(input)
        }
        
        // Add video input
        switch withCaptureDevicePosition {
        case .front:
            if let _frontDeviceInput = frontDeviceInput {
                session.addInput(_frontDeviceInput)
            }
        default:
            if let _defaultDeviceInput = defaultDeviceInput {
                session.addInput(_defaultDeviceInput)
            }
        }
    }
    
    private func startCameraSession() {
        if session.inputs.count > 0 {
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            session.addOutput(metadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = supportedCodeTypes
            //            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
        }
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        // Start video capture.
        session.startRunning()
        
    }
    
    private func layoutComponents() {
        self.exitButton.translatesAutoresizingMaskIntoConstraints = false
        self.exitButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.exitButton.widthAnchor.constraint(equalTo: self.exitButton.heightAnchor, multiplier: 1).isActive = true
        self.exitButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        self.exitButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -20).isActive = true
        
        self.switchCameraButton.translatesAutoresizingMaskIntoConstraints = false
        self.switchCameraButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.switchCameraButton.widthAnchor.constraint(equalTo: self.switchCameraButton.heightAnchor, multiplier: 1).isActive = true
        self.switchCameraButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        self.switchCameraButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 20).isActive = true
        
        self.toggleTorchButton.translatesAutoresizingMaskIntoConstraints = false
        self.toggleTorchButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.toggleTorchButton.widthAnchor.constraint(equalTo: self.toggleTorchButton.heightAnchor, multiplier: 1).isActive = true
        self.toggleTorchButton.bottomAnchor.constraint(equalTo: self.switchCameraButton.bottomAnchor, constant: 0).isActive = true
        self.toggleTorchButton.leftAnchor.constraint(equalTo: self.switchCameraButton.rightAnchor, constant: 20).isActive = true
    }
    
    
    private func updatePreviewLayer(layer: AVCaptureConnection, orientation: AVCaptureVideoOrientation) {
        layer.videoOrientation = orientation
        videoPreviewLayer?.frame = self.view.bounds
    }
    
    
    @objc func switchCameraAction(_ button: UIButton) {
        if let _frontDeviceInput = frontDeviceInput {
            session.beginConfiguration()
            
            if let _currentInput = session.inputs.first as? AVCaptureDeviceInput {
                session.removeInput(_currentInput)
                
                let newDeviceInput = (_currentInput.device.position == .front) ? defaultDeviceInput : _frontDeviceInput
                session.addInput(newDeviceInput!)
            }
            
            session.commitConfiguration()
        }
    }
    
    @objc func toggleTorchAction(_ button: UIButton) {
        do {
            try defaultDevice?.lockForConfiguration()
            
            defaultDevice?.torchMode = defaultDevice?.torchMode == .on ? .off : .on
            if defaultDevice?.torchMode == .on {
                button.setImage(UIImage(named: "ic_flash_off"), for: .normal)
            } else {
                button.setImage(UIImage(named: "ic_flash_on"), for: .normal)
            }
            defaultDevice?.unlockForConfiguration()
        }
        catch _ { }
    }
    
    @objc func toggleExitAction(_ button: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension QRCodeScannerController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata (or barcode) then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            guard let rectOfInterest = rectOfInterest, let qrCodeFrame = qrCodeFrameView?.frame , rectOfInterest.contains(qrCodeFrame) else {
                return
            }
            if let result = metadataObj.stringValue {
                delegate?.didFindQRCode(result: result)
                print("DID FIND CODE: \(result)")
                if self.stopScanningWhenCodeIsFound {
                    self.session.stopRunning()
                }
                let vc = UIAlertController(title: "Did find QR Code", message: result, preferredStyle: .alert)
                vc.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    vc.dismiss(animated: true, completion: nil)
                }))
                vc.addAction(UIAlertAction(title: "Cancle", style: .default, handler: { (_) in
                    self.session.startRunning()
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
}

//
//  OverlayView.swift
//  QRCodeReader
//
//  Created by HieuVD on 12/19/19.
//  Copyright © 2019 Vnext Da Nang. All rights reserved.
//

import Foundation
import UIKit
class OverlayView: UIView {
    var overlayColor = UIColor.gray.withAlphaComponent(0.3)
    var angledColor = UIColor.green.withAlphaComponent(0.3)
    var isDrawAngled = true
    private var lineSize: CGFloat = 10
    private var focusRect: CGRect? {
        didSet {
            drawOverlayLayer()
        }
    }
    convenience init(frame: CGRect? = nil, focusRect: CGRect?) {
        self.init(frame: frame ?? CGRect.zero )
      self.focusRect = focusRect
    }
    
    override init(frame: CGRect) {
      super.init(frame: frame)
      setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      setupUI()
    }
    
    private func setupUI() {
      self.layer.cornerRadius = 5
    }
     
    override func layoutSubviews() {
        super.layoutSubviews()
        drawOverlayLayer()
        drawAngled()
    }
    
    private func drawOverlayLayer() {
        guard let rect = focusRect else {
            return
        }
        let shape = CAShapeLayer()
        self.layer.addSublayer(shape)
        shape.opacity = 0.6
        shape.lineWidth = 0
        shape.fillColor = overlayColor.cgColor
        let bounds = self.frame
        print(bounds.minY)
        let path = UIBezierPath()
        path.move(to: CGPoint(x: bounds.minX, y: bounds.minY))
        path.addLine(to: CGPoint(x: bounds.maxX, y: bounds.minY))
        path.addLine(to: CGPoint(x: bounds.maxX, y: rect.minY))
        path.addLine(to: CGPoint(x: bounds.minX, y: rect.minY))
        path.close()
        path.move(to: CGPoint(x: bounds.minX, y: bounds.maxY))
           path.addLine(to: CGPoint(x: bounds.maxX, y: bounds.maxY))
           path.addLine(to: CGPoint(x: bounds.maxX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        path.addLine(to: CGPoint(x: bounds.minX, y: bounds.minY+rect.minY))
        path.close()
        shape.path = path.cgPath
    }
    
    private func drawAngled() {
        guard let rect = focusRect, isDrawAngled else {
            return
        }
        let shape = CAShapeLayer()
        self.layer.addSublayer(shape)
        shape.lineWidth = 2
        shape.strokeColor = angledColor.cgColor
        shape.fillColor = UIColor.clear.cgColor
        let bounds = self.frame
        print(bounds.minY)
        let path = UIBezierPath()
        // top left
        path.move(to: CGPoint(x: rect.minX, y: rect.minY + lineSize))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX + lineSize, y: rect.minY))
        
        // top right
        path.move(to: CGPoint(x: rect.maxX - lineSize, y: rect.minY ))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY + lineSize))
        
        // bottom left
        path.move(to: CGPoint(x: rect.minX, y: rect.maxY - lineSize))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX + lineSize, y: rect.maxY))
        
        // bottom right
        path.move(to: CGPoint(x: rect.maxX - lineSize, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY - lineSize))
        shape.path = path.cgPath
    }
}
